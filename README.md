# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/blob/main/README.md?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/blob/main/README.md?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/blob/main/README.md?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like.

---
## Python Files and Projects

In this repository I will be posting my Python files and Python projects.

---
#### Simple Python Program.

Off course we have to start with the world famous **Hello, World!** program:

My code:

```python
# ------ Document info ------

#   Filename: hello_world.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 26th April 2024
#   Purpose: Print "Hello, World!" in the Terminal.


# ------ Imports ------


# ------ Constants & Variables ------
text: str = 'Hello, World!'


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print(text)


if __name__ == '__main__':
    main()
```

This code will provide us with the following output:

![Hello, World!](/img/hello_world.png)

---
#### [Projects](https://gitlab.com/ph179/python/-/blob/main/projects?ref_type=heads):

1. [Computer Quiz](https://gitlab.com/ph179/python/-/tree/main/projects/computer_quiz?ref_type=heads) - (Easy)

2. [Number Guessing Game](https://gitlab.com/ph179/python/-/tree/main/projects/number_guessing_game?ref_type=heads) - (Easy)

3. [Rock - Paper - Scissors]() - (Easy)
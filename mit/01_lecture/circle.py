# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 7th July 2024
#   Last modified: 
#   Purpose: Area and Circumference of a circle.


# ------ Imports ------


# ------ Constants & Variables ------
pi = 355 / 113
radius = 2.2


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """


if __name__ == '__main__':
    main()
# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like.

---
## Computer Science and Programming Using Python

Today I started following this MIT course. It looks interesting and has got many positive comments.

The course is one lecture a week. So I will be doing the same:

    - Look at a video
    - Document what is told
    - Make the exercises

It will be a one week / one video sequel.

---
### Online Video

[YouTube Video](https://www.youtube.com/watch?v=xAcTmDO6NTI&list=PLUl4u3cNGP62A-ynp6v6-LGBCzeH3VAQB&index=1)

---
### Python Console

![Type Integer](img/type_integer.png)

![Type Float](img/type_float.png)

![Casting: Float](img/casting_float.png)

![Casting: Integer](img/casting_integer.png)

![Rounding: Float](img/rounding_float.png)

![Expression](img/expression.png)

![Expression](img/expression_0.png)

![Expression](img/expression_1.png)

![Expression](img/expression_2.png)
# cube = 27         # While loop 1.
# cube = 8120601    # While loop 1.
cube = 10000        # Infinite loop! While loop 2 to skip it.
epsilon = 0.01
guess = 0.0
increment = 0.01
num_guesses = 0


# while abs(guess ** 3 - cube) >= epsilon:
while (abs(guess ** 3 - cube) >= epsilon) and guess <= cube:
    guess += increment
    num_guesses += 1


print('Number of guesses:', num_guesses)


if abs(guess ** 3 - cube) >= epsilon:
    print('Failed on cube root of', cube)
else:
    print(guess, 'is close to the cube root of', cube)
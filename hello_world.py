# ------ Document info ------

#   Filename: hello_world.py
#   Author: Philippe Heyvaert
#   Created: 26th April 202
#   Last modified: 26th April 2024
#   Purpose: Print "Hello, World!" in the Terminal.


# ------ Imports ------


# ------ Constants & Variables ------
text: str = 'Hello, World!'


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print(text)


if __name__ == '__main__':
    main()
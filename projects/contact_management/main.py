# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 27th September 2024
#   Last modified: 29th September 2024
#   Purpose: Manage contacts.


# ------ Imports ------
import json


# ------ Constants & Variables ------


# ------ Functions ------
def add_person() -> dict:
    """
    Add a person to the contact list.
    
    :return: dict
    """
    name: str = input('Name: ')
    age: str = input('Age: ')
    email: str = input('Email: ')

    person: dict = {
        "name": name,
        "age": age,
        "email": email
    }

    return person


def display_people(people: list) -> None:
    """
    Display the contacts in the list.
    
    :return:
    """
    for i, person in enumerate(people):
        print(i + 1, "-", person["name"], "|", person["age"], "|", person["email"])


def delete_person(people: list) -> None:
    """
    Delete a person from the contact list.
    
    :return:
    """
    display_people(people=people)
    
    while True:
        number: str = input('Enter a number to delete: ')

        try:
            number = int(number)

            if number <= 0 or number > len(people):
                print('Invalid number, out of range! Please try again.')
            else:
                break
        except:
            print('Invalid number. Please try again.')
    
    people.pop(number - 1)


def search(people: list) -> None:
    """
    Search for a person in the contact list.
    
    :return:
    """
    search_name: str = input('Search for a name: ').lower()
    results: list = []

    for person in people:
        name: str = person["name"]

        if search_name in name.lower():
            results.append(person)

    display_people(people=results)


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print()
    print("Welcome to the Contact Management System!")
    print()

    with open("contacts.json", "r") as file:
        people = json.load(file)["contacts"]

    while True:
        print('Contact list size:', len(people))
        command: str = input("You can 'Add', 'Delete' or 'Search'. 'Q' to quit: ").lower()

        if command == 'add':
            people.append(add_person())
            print('Person added to the list.')
        elif command == 'delete':
            delete_person(people=people)
            print('Person deleted from the list.')
        elif command == 'search':
            search(people=people)
        elif command == 'q':
            break
        else:
            print('Invalid command.')

    with open("contacts.json", "w") as file:
        json.dump({"contacts": people}, file)


if __name__ == '__main__':
    main()
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 25th September 2024
#   Last modified: 26th September 2024
#   Purpose: Adventure game to learn Python Basics.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """


if __name__ == '__main__':
    main()
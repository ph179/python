# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 1st May 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------
QUESTIONS: int = 10


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')

    if playing.lower() != "yes":
        quit()

    print('Okay! Let\'s play.')
    score: int = 0

    answer: str = input('What does CPU stand for? ')
    if answer.lower() == 'central processing unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does GPU stand for? ')
    if answer.lower() == 'graphics processing unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does RAM stand for? ')
    if answer.lower() == 'random access memory':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does PSU stand for? ')
    if answer.lower() == 'power supply unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What is the smallest unit of memory? ')
    if answer.lower() == 'bit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does SSD stand for? ')
    if answer.lower() == 'solid state device':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What are physical devices of a computer called? ')
    if answer.lower() == 'hardware':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does ROM stand for? ')
    if answer.lower() == 'read only memory':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What is Python? ')
    if answer.lower() == 'programming language':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does I/O stand for? ')
    if answer.lower() == 'input/output':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')
    
    print('You got ' + str(score) + ' questions correct.')
    print('You got ' + str((score / QUESTIONS) * 100) + '%.')


if __name__ == '__main__':
    main()
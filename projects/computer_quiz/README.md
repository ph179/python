# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like.

---
## Python Files and Projects

In this repository I will be posting my Python files and Python projects.

---
## Tech With Tim - Project #1:

In this project, we will create a simple **Quiz game**.

The specifications for this project can be found in the [Project Objectives](#project-objectives) section of this document.

In the [Project](#project) section, we will take a step-by-step approach to every part of this project.

In the [Project Conclusion](#project-conclusion), there will be an overview of what we have [learned](#what-have-we-learned-while-doing-this-project-quiz-game) while doing this project. We will also look at any [future improvements](#future-project-improvements) for this project.

---
### YouTube Video: 

[9 HOURS of Python Projects - From Beginner to Advanced](https://www.youtube.com/watch?v=NpmFbWO6HPU&t=19243s)

---
### Project Objectives:

1. Ask the user questions.
2. Get user input to answer the questions.
3. Check if the user answer is right or wrong.
4. Keep score on the questions.

---
### Project:

1. Greet the user and ask the user if he wants to play our quiz game. For testing reasons we print out the **variable** ```playing```. We want to see what is stored in this **variable**.

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 29th April 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')
    print(playing)


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input.png)

2. Does the user wants to play? We will be checking this with an **if-statement**.

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 30th April 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')
    # print(playing)        # Testing.

    if playing != "yes":
        quit()

    print('Okay! Let\'s play.')


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input_0.png)

3. Ask a question and check if the answer is correct using an **if-else-statement**.

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 1st May 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')
    # print(playing)        # Testing.

    if playing != "yes":
        quit()

    print('Okay! Let\'s play.')

    answer: str = input('What does CPU stand for? ')
    if answer == 'central processing unit':
        print('Correct!')
    else:
        print('Incorrect!')


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input_1.png)

4. Add the other questions (copy-and-paste). I've prepared 10 questions in total for this quiz. Feel free to use other questions.

- Q: What does CPU stand for? - A: Central Processing Unit

- Q: What does GPU stand for? - A: Graphics Processing Unit

- Q: What does RAM stand for? - A: Random Access Memory

- Q: What does PSU stand for? - A: Power Supply Unit

- Q: What is the smallest unit of memory? - A: Bit

- Q: What does SSD stand for? - A: Solid State Device

- Q: What are physical devices of a computer called? - A: Hardware

- Q: What does ROM stand for? - A: Read Only Memory

- Q: What is Python? - A: Programming Language

- Q: What does I/O stand for? - A: Input/Output

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 1st May 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')
    # print(playing)        # Testing.

    if playing != "yes":
        quit()

    print('Okay! Let\'s play.')

    answer: str = input('What does CPU stand for? ')
    if answer == 'central processing unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does GPU stand for? ')
    if answer == 'graphics processing unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does RAM stand for? ')
    if answer == 'random access memory':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does PSU stand for? ')
    if answer == 'power supply unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What is the smallest unit of memory? ')
    if answer == 'bit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does SSD stand for? ')
    if answer == 'solid state device':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What are physical devices of a computer called? ')
    if answer == 'hardware':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does ROM stand for? ')
    if answer == 'read only memory':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What is Python? ')
    if answer == 'programming language':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does I/O stand for? ')
    if answer == 'input/output':
        print('Correct!')
    else:
        print('Incorrect!')


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input_2.png)

5. Problems detected while testing the program:

- When we use 'Yes' with a capital the program just quits, because "yes" is not equal to "Yes".

![Result in our terminal](img/quiz_game_input_3.png)

- We also see a problem when we use one or more capitals in the answer, the program states incorrect even if the contents of the answer with capitals is correct. We will have to fix both of these problems.

![Result in our terminal](img/quiz_game_input_4.png)

To fix this problem we have to use the **method** ```lower()```.

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 1st May 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')

    if playing.lower() != "yes":
        quit()

    print('Okay! Let\'s play.')

    answer: str = input('What does CPU stand for? ')
    if answer.lower() == 'central processing unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does GPU stand for? ')
    if answer.lower() == 'graphics processing unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does RAM stand for? ')
    if answer.lower() == 'random access memory':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does PSU stand for? ')
    if answer.lower() == 'power supply unit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What is the smallest unit of memory? ')
    if answer.lower() == 'bit':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does SSD stand for? ')
    if answer.lower() == 'solid state device':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What are physical devices of a computer called? ')
    if answer.lower() == 'hardware':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does ROM stand for? ')
    if answer.lower() == 'read only memory':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What is Python? ')
    if answer.lower() == 'programming language':
        print('Correct!')
    else:
        print('Incorrect!')

    answer: str = input('What does I/O stand for? ')
    if answer.lower() == 'input/output':
        print('Correct!')
    else:
        print('Incorrect!')


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input_5.png)

6. Adding score to the quiz game.

Code:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th April 2024
#   Last modified: 1st May 2024
#   Purpose: Ask questions, get answers and check them.


# ------ Imports ------


# ------ Constants & Variables ------
QUESTIONS: int = 10


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    print('Welcome to my computer quiz.')

    playing: str = input('Do you want to play? ')

    if playing.lower() != "yes":
        quit()

    print('Okay! Let\'s play.')
    score: int = 0

    answer: str = input('What does CPU stand for? ')
    if answer.lower() == 'central processing unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does GPU stand for? ')
    if answer.lower() == 'graphics processing unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does RAM stand for? ')
    if answer.lower() == 'random access memory':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does PSU stand for? ')
    if answer.lower() == 'power supply unit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What is the smallest unit of memory? ')
    if answer.lower() == 'bit':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does SSD stand for? ')
    if answer.lower() == 'solid state device':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What are physical devices of a computer called? ')
    if answer.lower() == 'hardware':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does ROM stand for? ')
    if answer.lower() == 'read only memory':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What is Python? ')
    if answer.lower() == 'programming language':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')

    answer: str = input('What does I/O stand for? ')
    if answer.lower() == 'input/output':
        print('Correct!')
        score += 1
    else:
        print('Incorrect!')
    
    print('You got ' + str(score) + ' questions correct.')
    print('You got ' + str((score / QUESTIONS) * 100) + '%.')


if __name__ == '__main__':
    main()
```

Result in our terminal:

![Result in our terminal](img/quiz_game_input_6.png)

7. The program works fine now. So I'll be looking at those future improvements next. Maybe I can make it shorter with a function or two.

---
### Project Conclusion:

#### What have we learned while doing this project **Quiz game**?

- We've learned how to use the **input method**.

- We've learned about **if-statements**.

- We've learned about **if-else-statements**.

- We've learned about the **method** ```lower()```.

- We've learned how to use a constant.

#### Future project improvements:

- Create a Quiz framework with functions and load the questions and answers from an external .txt file. Applying this we can create multiple sets of questions and answers. Capitals, countries... etc.

- Ask the user how many questions he wants.

- Clean up the terminal screen, so the quiz game looks smooth.

- Do not quit the game without greeting the user.

## [Projects](https://gitlab.com/ph179/python/-/blob/main/projects?ref_type=heads):

1. [Computer Quiz](https://gitlab.com/ph179/python/-/tree/main/projects/computer_quiz?ref_type=heads) - (Easy)

2. [Number Guessing Game](https://gitlab.com/ph179/python/-/tree/main/projects/number_guessing_game?ref_type=heads) - (Easy)
# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)
My large wallpaper collection, feel free to browse my collection and download what you like.

---
## Python Files and Projects

Here are my projects.

---
### [Projects](https://gitlab.com/ph179/python/-/blob/main/projects?ref_type=heads):

1. [Computer Quiz](https://gitlab.com/ph179/python/-/tree/main/projects/computer_quiz?ref_type=heads) - (Easy)

2. [Number Guessing Game](https://gitlab.com/ph179/python/-/tree/main/projects/number_guessing_game?ref_type=heads) - (Easy)
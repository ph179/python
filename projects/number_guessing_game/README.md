# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like.

---
## Python Files and Projects

In this repository I will be posting my Python files and Python projects.

---
## Tech With Tim - Project #2:

In this project, we will create a simple **Number Guessing Game**.

The specifications for this project can be found in the [Project Objectives](#project-objectives) section of this document.

In the [Project](#project) section, we will take a step-by-step approach to every part of this project.

In the [Project Conclusion](#project-conclusion), there will be an overview of what we have [learned](#what-have-we-learned-while-doing-this-project-number-guessing-game) while doing this project. We will also look at any [future improvements](#future-project-improvements) for this project.

---
### YouTube Video: 

[9 HOURS of Python Projects - From Beginner to Advanced](https://www.youtube.com/watch?v=NpmFbWO6HPU&t=19243s)

---
### Project Objectives:

1. Ask the user for a number. We use this number as the top of our ``randrange()`` method.

2. We check if the user input is a number. The number has to be greater than 0. To do this we use the ``isdigit()`` method. We prompt the user if his input isn't a number.

3. We use a **while loop** to let the user guess the number. Again we check if the input is a number. We use the **keyword** ``continue`` until the user enters a number or guesses our computer random selected number.

4. Display how many guesses the user needed after the game.

---
### Project:

1. When you use the **random module**, you can select a random number within a range. There are 2 ways to do this.

    1. Use the ``randrange(start, stop)`` method to define a range between the *start* and *stop* interval. The **variables** *start* and *stop* are replaced by numbers, **constants** or a **function** return value if it is a number value. Be aware that *stop* is ***NOT*** included. Please note that *start* isn't required.

    Example:

    Code:

    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 6th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------
    random_number: int = random.randrange(0, 11)    # Random select a number.


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        print(random_number)


    if __name__ == '__main__':
        main()
    ```

    Numbers between 0 and 10 are generated at random with this program.

    ![Result in our terminal](img/number_game.png)

    2. Use the ``randint(start, stop)`` method to define a range between the *start* and *stop* interval. The **variables** *start* and *stop* are replaced by numbers, **constants** or a **function** return value if it is a number value.

    Example:

    Code:

    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 6th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------
    random_number: int = random.randint(0, 11)    # Random select a number.


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        print(random_number)


    if __name__ == '__main__':
        main()
    ```

    Numbers between 0 and 10 are generated at random with this program.

    ![Result in our terminal](img/number_game_0.png)

2. We ask the user for a number and check the input 2 ways:

    - Is the input a number? To do this we use the ``ìsdigit()`` method. If so we convert the input **string** to an **int** with the ``ìnt()`` method. If the input isn't a number we quit the program.

    - Is the input larger than 0? If the number is 0 or less we quit the program.

    Code:

    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 7th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        top_of_range: str = input('Please, type a number: ')
        if top_of_range.isdigit():
            top_of_range = int(top_of_range)
            if top_of_range <= 0:
                print('Please give a number larger than 0 next time.')
                quit()
        else:
            print('Please enter a number next time.')
            quit()

        random_number: int = random.randint(0, top_of_range)
        print(random_number)


    if __name__ == '__main__':
        main()
    ```

    Result:

    ![Result in our terminal](img/number_game_1.png)

3. Now we let the user guess the random number. For this we use a **while-loop**.

    - Is the input a number? Again, we use the ``ìsdigit()`` method. If it isn't a number we use the ``continue`` **keyword** to go back at the beginning of the **while-loop**.

    - We use the ``break`` **keyword** if the guess is correct and we quit the program.

    Code:


    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 11th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        top_of_range: str = input('Please, type a number: ')
        if top_of_range.isdigit():
            top_of_range = int(top_of_range)
            if top_of_range <= 0:
                print('Please give a number larger than 0 next time.')
                quit()
        else:
            print('Please enter a number next time.')
            quit()
        
        random_number: int = random.randint(0, top_of_range)

        while True:
            user_guess: str = input('Please, make a guess: ')
            if user_guess.isdigit():
                user_guess = int(user_guess)
            else:
                print('Please enter a number next time.')
                continue

            if user_guess == random_number:
                print('You got it!')
                break
            else:
                print('You got it wrong!')


    if __name__ == '__main__':
        main()
    ```

    Result:

    ![Result in our terminal](img/number_game_2.png)

4. Next step, we are going to keep track of the number of guesses the user makes. After the game we print the statistics for the user in the terminal.

    Code:

    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 11th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        top_of_range: str = input('Please, type a number: ')
        if top_of_range.isdigit():
            top_of_range = int(top_of_range)
            if top_of_range <= 0:
                print('Please give a number larger than 0 next time.')
                quit()
        else:
            print('Please enter a number next time.')
            quit()
        
        random_number: int = random.randint(0, top_of_range)
        guesses: int = 0

        while True:
            guesses += 1
            user_guess: str = input('Please, make a guess: ')
            if user_guess.isdigit():
                user_guess = int(user_guess)
            else:
                print('Please enter a number next time.')
                continue

            if user_guess == random_number:
                print('You got it!')
                break
            else:
                print('You got it wrong!')
        
        print(f"You got it in {guesses} guesses.")


    if __name__ == '__main__':
        main()
    ```

    Result:

    ![Result in our terminal](img/number_game_3.png)

5. With a small number this game doesn't need an indication for the user about his guess. But if the user decides to use a big start number (e.g. 1000), it would be nice to indicate to the user if his guess is below or above the random number picked by the computer.

    - We are going to use an **if-elif-else** statement for this. This will give us a clean code.

    Code:

    ```python
    # ------ Document info ------

    #   Filename: main.py
    #   Author: Philippe Heyvaert
    #   Created: 3rd May 2024
    #   Last modified: 12th May 2024
    #   Purpose: Create a number guessing game


    # ------ Imports ------
    import random


    # ------ Constants & Variables ------


    # ------ Functions ------


    # ------ Main program ------
    def main() -> None:
        """
        This main() function runs all of the main program.

        :return:
        """
        top_of_range: str = input('Please, type a number: ')
        if top_of_range.isdigit():
            top_of_range = int(top_of_range)
            if top_of_range <= 0:
                print('Please give a number larger than 0 next time.')
                quit()
        else:
            print('Please enter a number next time.')
            quit()
        
        random_number: int = random.randint(0, top_of_range)
        guesses: int = 0

        while True:
            guesses += 1
            user_guess: str = input('Please, make a guess: ')
            if user_guess.isdigit():
                user_guess = int(user_guess)
            else:
                print('Please enter a number next time.')
                continue

            if user_guess == random_number:
                print('You got it!')
                break
            elif user_guess > random_number:
                print('You were above the number!')
            else:
                print('You were below the number')
        
        print(f"You got it in {guesses} guesses.")


    if __name__ == '__main__':
        main()
    ```

    Result:

    ![Result in our terminal](img/number_game_4.png)

---
### Project Conclusion:

#### What have we learned while doing this project **Number Guessing Game**?

- Importing standard modules like ``random``.

- Using the ``random`` module.

- Using the ``isdigit()`` method.

- Using the **keywords** ``continue`` and ``break`` in a **while-loop**.

- Using an **if-elif-else** statement.

#### Future project improvements:

- Ask the user for her or his name before starting the game. Maybe we can use a **function** for this.

- Cleanup the screen to ensure a smooth look of the game.

## [Projects](https://gitlab.com/ph179/python/-/blob/main/projects?ref_type=heads):

1. [Computer Quiz](https://gitlab.com/ph179/python/-/tree/main/projects/computer_quiz?ref_type=heads) - (Easy)

2. [Number Guessing Game](https://gitlab.com/ph179/python/-/tree/main/projects/number_guessing_game?ref_type=heads) - (Easy)
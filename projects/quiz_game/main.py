# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 29th September 2024
#   Last modified: 30th September 2024
#   Purpose: Quiz game.


# ------ Imports ------
import random
import json
import os
import time


# ------ Constants & Variables ------


# ------ Functions ------
def clear() -> None:
    """
    Clear the terminal. This function works for all systems:
        - GNU/Linux
        - MacOs
        - Windows
    
    :return:
    """
    os.system('cls' if os.name == 'nt' else 'clear')


def load_questions() -> dict:
    """
    Load questions from the .json file.
    
    :return: dict
    """
    with open("questions.json", "r") as file:
        questions: dict = json.load(file)["questions"]
    
    return questions


def get_random_questions(questions: dict, number_questions: int) -> dict:
    """
    Select random questions from the list of questions.
    
    :return:
    """
    if number_questions > len(questions):
        number_questions = len(questions)
    
    random_questions: dict = random.sample(questions, number_questions)

    return random_questions


def ask_question(question) -> bool:
    """
    Ask one question to the user.
    
    :return: bool
    """
    print(question["question"])
    print()

    for i, option in enumerate(question["options"]):
        print(str(i + 1) + ".", option)

    number = int(input("Select the correct number (1-4): "))

    if number < 1 or number > len(question["options"]):
        print("Invalid choice, defaulting to wrong answer.")

        return False

    correct = question["options"][number - 1] == question["answer"]

    return correct


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    questions: dict = load_questions()
    possible_questions: int = len(questions)
    total_questions: int = int(input(f'Enter the number of questions (maximum = {possible_questions}): '))
    random_questions: dict = get_random_questions(questions, total_questions)
    correct: int = 0
    start_time = time.time()

    clear()

    for question in random_questions:
        is_correct = ask_question(question)

        if is_correct:
            correct += 1

        print()
    
    completed_time = time.time() - start_time
    
    clear()

    print('Quiz Summary:')
    print()
    print(f'Total questions: {total_questions}')
    print(f'Correct answers: {correct}')
    print(f'Score: {round((correct / total_questions) * 100, 2)}%')
    print(f'Time: you took {round(completed_time, 2)} seconds')
    

if __name__ == '__main__':
    main()
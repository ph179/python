# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 19th September 2024
#   Last modified: 23rd September 2024
#   Purpose: Simulate Las Vegas slotmachine.


# ------ Imports ------
import random


# ------ Constants & Variables ------
MAX_LINES: int = 3
MAX_BET: int = 100
MIN_BET: int = 1

ROWS: int = 3
COLS: int = 3

symbol_count: dict = {
    "💣": 2,
    "🔔": 4,
    "🪙": 6,
    "♥️ ": 8
}

symbol_value = {
    "💣": 5,
    "🔔": 4,
    "🪙": 3,
    "♥️ ": 2
}


# ------ Functions ------
def check_winnings(columns, lines: int, bet: int, values) -> int:
    """
    Check winnings.
    
    :return:
    """
    winnings = 0

    for line in range(lines):
        symbol = columns[0][line]
        for column in columns:
            symbol_to_check = column[line]
            if symbol != symbol_to_check:
                break
            else:
                winnings += values[symbol] * bet
    
    return winnings


def get_slot_machine_spin(rows: int, cols: int, symbols: dict) -> list:
    """
    Simulate a slot machine spin.
    
    :return: list
    """
    all_symbols = []

    for symbol, symbol_count in symbols.items():
        for _ in range(symbol_count):
            all_symbols.append(symbol)
    
    columns = []

    for _ in range(cols):
        column = []
        current_symbols = all_symbols[:]
        for _ in range(rows):
            value = random.choice(current_symbols)
            current_symbols.remove(value)
            column.append(value)
        
        columns.append(column)
    
    return columns


def print_slot_machine(columns: list) -> None:
    """
    Print the slotmachine spin: transposing rows into columns.
    
    :return:
    """
    for row in range(len(columns[0])):
        for i, column in enumerate(columns):
            if i != len(columns) - 1:
                print(column[row], end=" | ")
            else:
                print(column[row], end="")
        print()


def deposit() -> int:
    """
    Track the amount the user deposits for the game.
    
    :return: int
    """
    while True:
        amount = input('What would you like to deposit? €')

        if amount.isdigit():
            amount = int(amount)
            if amount > 0:
                break
            else:
                print('The amount must be greater than 0.')
        else:
            print('Please enter a number.')
    
    return amount


def get_number_of_lines() -> int:
    """
    Get the number of lines the user is betting on.
    
    :return: int
    """
    while True:
        lines = input('Bet on how many line(s) (1-' + str(MAX_LINES) + ')? ')

        if lines.isdigit():
            lines = int(lines)
            if 1 <= lines <= MAX_LINES:
                break
            else:
                print('Enter a valid number of line(s).')
        else:
            print('Please enter a number.')
    
    return lines


def get_bet() -> int:
    """
    Get bet from the user.
    
    :return: int
    """
    while True:
        amount = input('What would you like to bet on each line? €')

        if amount.isdigit():
            amount = int(amount)
            if MIN_BET <= amount <= MAX_BET:
                break
            else:
                print(f'The amount must be between €{MIN_BET} - €{MAX_BET}.')
        else:
            print('Please enter a number.')
    
    return amount


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    balance = deposit()
    lines = get_number_of_lines()
    
    while True:
        bet = get_bet()
        total_bet = bet * lines

        if total_bet > balance:
            print(f'Not enough money in your balance! Balance: €{balance}.')
        else:
            break

    print(f'Bet: €{bet} on {lines} lines. Total bet: €{total_bet}.')
    
    slots = get_slot_machine_spin(ROWS, COLS, symbol_count)
    print_slot_machine(slots)
    winnings = check_winnings(slots, lines, bet, symbol_value)
    print(f'You won €{winnings}.')


if __name__ == '__main__':
    main()
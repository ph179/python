# Shooting game

New project... this will be fun!

---
## Project

Trying to make an aim-and-shoot game.

Inspiration:

[Qwak!](https://nl.wikipedia.org/wiki/Qwak!)

[Duck Hunt](https://nl.wikipedia.org/wiki/Duck_Hunt)

---
## Challenge

Explore ```pygame``` some more. How to implement mouse click events.

---
## Project rating

Advanced.

## Code progress & Video's

1. Let's look at my ```pygame window```. Video below my code.

My code so far:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 06th October 2024
#   Last modified: 07th October 2024
#   Purpose: Silly aiming and shoot game.


# ------ Imports ------
import math
import random
import time
import pygame
pygame.init()


# ------ Constants & Variables ------
WIDTH = 800
HEIGHT = 600

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Shoot'em up! Game")


# ------ Functions ------
def game():
    """
    Game loop.
    
    :return:
    """
    run = True

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

    pygame.quit()


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    game()


if __name__ == '__main__':
    main()
```

![](img/pygame_window.mp4)

2. Building a ```class Target```. And creating a seperate function to draw 
in our ```pygame window```.

My code so far:

```python
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 06th October 2024
#   Last modified: 07th October 2024
#   Purpose: Silly aiming and shoot game.


# ------ Imports ------
import math
import random
import time
import pygame
pygame.init()


# ------ Constants & Variables ------
WIDTH = 800
HEIGHT = 600

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Shoot'em up! Game")

# Smaller number means that the difficulty increases.
TARGET_INCREMENT = 500
TARGET_EVENT = pygame.USEREVENT

# Window border offset for targets - Test this setting later.
TARGET_PADDING = 30


# ------ Classes ------
class Target:
    # ------ Class Constants & Variables ------
    MAX_SIZE = 30
    GROWTH_RATE = 0.2
    FIRST_COLOR = "red"
    SECOND_COLOR = "white"


    def __init__(self, x, y):
        """
        Class Target constructor.
        
        :return:
        """
        self.x = x
        self.y = y
        self.size = 0
        self.grow = True
    

    def update(self):
        """
        Check if the target(s) should grow or shrink.
        
        :return:
        """
        if self.MAX_SIZE + self.GROWTH_RATE >= self.MAX_SIZE:
            self.grow = False
        
        if self.grow:
            # Make the target grow.
            self.size += self.GROWTH_RATE
        else:
            # Make the target shrink.
            self.size -= self.GROWTH_RATE
    

    def draw(self, window):
        """
        Draw the targets on the screen.
        
        :return:
        """
        # Draw the first circle in red.
        pygame.draw.circle(WIN, self.FIRST_COLOR, (self.x, self.y), 
                           self.size)
        # Draw an overlapping, smaller circle in white.
        pygame.draw.circle(WIN, self.SECOND_COLOR, (self.x, self.y), 
                           self.size * 0.8)
        # Draw an overlapping, smaller circle in red.
        pygame.draw.circle(WIN, self.FIRST_COLOR, (self.x, self.y), 
                           self.size * 0.6)
        # Draw an overlapping, smaller circle in white.
        pygame.draw.circle(WIN, self.SECOND_COLOR, (self.x, self.y), 
                           self.size * 0.4)


# ------ Functions ------
def play_game():
    """
    Game loop.
    
    :return:
    """
    run = True
    targets = []

    pygame.time.set_timer(TARGET_EVENT, TARGET_INCREMENT)

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

            if event.type == TARGET_EVENT:
                x = random.randint(TARGET_PADDING, 
                                   WIDTH - TARGET_PADDING)
                y = random.randint(TARGET_PADDING, 
                                   HEIGHT - TARGET_PADDING)
                target = Target(x, y)
                targets.append(target)
                
    pygame.quit()


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    play_game()


if __name__ == '__main__':
    main()
```
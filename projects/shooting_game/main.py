# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 06th October 2024
#   Last modified: 07th October 2024
#   Purpose: Silly aiming and shoot game.


# ------ Imports ------
import math
import random
import time
import pygame
pygame.init()


# ------ Constants & Variables ------
WIDTH = 800
HEIGHT = 600

WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Shoot'em up! Game")

# Smaller number means that the difficulty increases.
TARGET_INCREMENT = 500
TARGET_EVENT = pygame.USEREVENT

# Window border offset for targets - Test this setting later.
TARGET_PADDING = 30

# Set our background color in RGB (Red-Green-Blue).
BG_COLOR = (0, 25, 40)


# ------ Classes ------
class Target:
    # ------ Class Constants & Variables ------
    MAX_SIZE = 30
    GROWTH_RATE = 0.2
    FIRST_COLOR = "red"
    SECOND_COLOR = "white"


    def __init__(self, x, y):
        """
        Class Target constructor.
        
        :return:
        """
        self.x = x
        self.y = y
        self.size = 0
        self.grow = True
    

    def update(self):
        """
        Check if the target(s) should grow or shrink.
        
        :return:
        """
        if self.MAX_SIZE + self.GROWTH_RATE >= self.MAX_SIZE:
            self.grow = False
        
        if self.grow:
            # Make the target grow.
            self.size += self.GROWTH_RATE
        else:
            # Make the target shrink.
            self.size -= self.GROWTH_RATE
    

    def draw(self, window):
        """
        Draw the targets on the screen.
        
        :return:
        """
        # Draw the first circle in red.
        pygame.draw.circle(window, self.FIRST_COLOR, (self.x, self.y), 
                           self.size)
        # Draw an overlapping, smaller circle in white.
        pygame.draw.circle(window, self.SECOND_COLOR, (self.x, self.y), 
                           self.size * 0.8)
        # Draw an overlapping, smaller circle in red.
        pygame.draw.circle(window, self.FIRST_COLOR, (self.x, self.y), 
                           self.size * 0.6)
        # Draw an overlapping, smaller circle in white.
        pygame.draw.circle(window, self.SECOND_COLOR, (self.x, self.y), 
                           self.size * 0.4)


# ------ Functions ------
def draw(window, targets):
    """
    Function to draw elements in our pygame window.

    :return:
    """
    window.fill(BG_COLOR)

    for target in targets:
        target.draw(window)
    
    pygame.display.update()
    

def play_game():
    """
    Game loop.
    
    :return:
    """
    run = True
    targets = []
    clock = pygame.time.Clock()

    pygame.time.set_timer(TARGET_EVENT, TARGET_INCREMENT)

    while run:
        clock.tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

            if event.type == TARGET_EVENT:
                x = random.randint(TARGET_PADDING, WIDTH - TARGET_PADDING)
                y = random.randint(TARGET_PADDING, HEIGHT - TARGET_PADDING)
                target = Target(x, y)
                targets.append(target)
        
        # Update targets before drawing them.
        for target in targets:
            target.update()
        
        draw(WIN, targets)
        # pygame.display.update()
                
    pygame.quit()


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    run = True
    targets = []
    clock = pygame.time.Clock()

    pygame.time.set_timer(TARGET_EVENT, TARGET_INCREMENT)

    while run:
        clock.tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

            if event.type == TARGET_EVENT:
                x = random.randint(TARGET_PADDING, WIDTH - TARGET_PADDING)
                y = random.randint(TARGET_PADDING, HEIGHT - TARGET_PADDING)
                target = Target(x, y)
                targets.append(target)
        
        # Update targets before drawing them.
        for target in targets:
            target.update()
        
        draw(WIN, targets)
        # pygame.display.update()
                
    pygame.quit()


if __name__ == '__main__':
    main()
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 24th September 2024
#   Last modified: 29th September 2024
#   Purpose: Tic tac toe game.


# ------ Imports ------
import os


# ------ Constants & Variables ------
X = '❌'
O = '⭕'

board: list = [
    [" ", " ", " "], 
    [" ", " ", " "], 
    [" ", " ", " "],
]


# ------ Functions ------
def clear() -> None:
    """
    Clear the terminal. This clear() function works for all systems:
        - GNU/Linux
        - MacOs
        - Windows
    
    :return:
    """
    os.system('cls' if os.name == 'nt' else 'clear')


def print_board(board: list) -> None:
    """
    Print the board.
    
    :return:
    """
    for i, row in enumerate(board):
        row_text = ' '
        for j, value in enumerate(row):
            row_text += value
            if j != len(row) - 1:
                row_text += ' | '
        print(row_text)

        if i != len(board) -1:
            print('-----------')


def get_number(text: str) -> int:
    """
    Check if the user input is valid.
    
    :return: int
    """
    while True:
        number = input(f'Please input the {text}: ')

        try:
            return int(number)
        except:
            print('Invalid number. Please, try again.')


def get_move(turn: str, board: list) -> None:
    """
    User input for the game.
    
    :return:
    """
    while True:
        row: int = get_number('row')
        col: int = get_number('column')

        if row < 1 or row > len(board):
            print('Invalid row, please try again.')
        elif col < 1 or col > len(board[row - 1]):
            print('Invalid column, please try again.')
        elif board[row - 1][col - 1] != " ":
            print('That position is already taken, please try again.')
        else:
            break

    board[row - 1][col - 1] = turn


def check_win(board: list, turn: str) -> bool:
    """
    Check which player has won.
    
    :return: bool
    """
    lines: list = [
        [(0, 0), (0, 1), (0, 2)],   # First row.
        [(1, 0), (1, 1), (1, 2)],   # Second row.
        [(2, 0), (2, 1), (2, 2)],   # Third row.
        [(0, 0), (1, 0), (2, 0)],   # First column.
        [(0, 1), (1, 1), (2, 1)],   # Second column.
        [(0, 2), (1, 2), (2, 2)],   # Third column.
        [(0, 0), (1, 1), (2, 2)],   # First diagonal.
        [(0, 2), (1, 1), (2, 0)]    # Second diagonal.
    ]

    for line in lines:
        win: bool = True
        for position in line:
            row, col = position

            if board[row][col] != turn:
                win = False
                break

        if win:
            return True
    
    return False


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    turn: str = 'X'
    turn_number: int = 0

    clear()
    print_board(board)

    while turn_number < 9:
        print()
        print(f'It is the {turn} players turn. Please select your move.')
        
        get_move(turn, board)
        
        print_board(board)
        winner: bool = check_win(board, turn)

        if winner:
            break

        if turn == 'X':
            turn = 'O'
        else:
            turn = 'X'

        turn_number += 1

    if turn_number == 9:
        print('Tied game')
    else:
        print('The winner was', turn)


if __name__ == '__main__':
    main()
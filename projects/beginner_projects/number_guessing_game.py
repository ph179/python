# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 11th September 2024
#   Last modified: 
#   Purpose: Guess a random generated number.


# ------ Imports ------
import random


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    top_of_range: str = input('Please, type a number: ')
    if top_of_range.isdigit():
        top_of_range = int(top_of_range)
        if top_of_range <= 0:
            print('Please give a number larger than 0 next time.')
            quit()
    else:
        print('Please enter a number next time.')
        quit()
    
    random_number: int = random.randint(0, top_of_range)
    guesses: int = 0

    while True:
        guesses += 1
        user_guess: str = input('Please, make a guess: ')
        if user_guess.isdigit():
            user_guess = int(user_guess)
        else:
            print('Please enter a number next time.')
            continue

        if user_guess == random_number:
            print('You got it!')
            break
        elif user_guess > random_number:
            print('You were above the number!')
        else:
            print('You were below the number')
    
    print(f"You got it in {guesses} guesses.")


if __name__ == '__main__':
    main()
# ------ Document info ------

#   Filename: dice_rolling_game.py.py
#   Author: Philippe Heyvaert
#   Created: 11th September 2024
#   Last modified: 
#   Purpose: Dice rolling simulation.


# ------ Imports ------
import random


# ------ Constants & Variables ------
die_a = None
die_b = None
thanks_string = 'Thank you for playing!'
error_string = 'This option is not valid! Please try again.'


# ------ Functions ------
def roll_dice() -> str:
    """
    This roll_dice() functions simulates rolling 2 dice.

    :return: Formatted string.
    """
    die_a = random.randint(1, 6)
    die_b = random.randint(1, 6)

    return print(f'({die_a}, {die_b})')


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
while True:
    roll = input('Do you want to roll the dice? (Yes/No): ').lower()
    if roll == 'yes' or roll =='y':
        roll_dice()
    elif roll == 'no' or roll =='n':
        print(thanks_string)
        break
    else:
        print(error_string)


if __name__ == '__main__':
    main()
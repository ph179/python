# ------ Document info ------

#   Filename: rock_paper_scissors.py
#   Author: Philippe Heyvaert
#   Created: 12th September 2024
#   Last modified: 15th September 2024
#   Purpose: Simulate the Rock-Paper-Scissors game.


# ------ Imports ------
import random
import os


# ------ Constants & Variables ------
ROCK = 'r'
SCISSORS = 's'
PAPER = 'p'

emojis = {ROCK: '🪨', SCISSORS: '✂️', PAPER: '🧾'}
choices = tuple(emojis.keys())

thanks_string = 'Thank you for playing!'
error_string = 'This option is not valid! Please try again.'

players = 1
legs = 0
sets = 3


# ------ Functions ------
def get_user_choice() -> str:
    """
    This get_user_choice() function ask the user for a choice: 
    Rock, Paper or Scissors.

    :return: str user_choice
    """
    while True:
        user_choice = input('Rock, paper or scissors? (r/p/s): ').lower()
        if user_choice in choices:
            return user_choice
        else:
            print(error_string)


def display_choices(user_choice, computer_choice) -> None:
    """
    This display_choices() function displays the choice as an emoji.

    :return:
    """
    print(f'You chose {emojis[user_choice]}')
    print(f'The computer chose {emojis[computer_choice]}')


def determine_winner(user_choice, computer_choice) -> None:
    """
    This determine_winner() function evaluates the different choices 
    and determines the winner (User or Computer).

    :return:
    """
    if user_choice == computer_choice:
        print('Tie!')
    elif (
        (user_choice == ROCK and computer_choice == SCISSORS) or 
        (user_choice == SCISSORS and computer_choice == PAPER) or 
        (user_choice == PAPER and computer_choice == ROCK)):
        print('You Win!')
    else:
        print('You lose!')


def play_game() -> None:
    """
    This play_game() function runs all functions in a while-loop to play 
    the game.

    :return:
    """
    while True:
        user_choice = get_user_choice()

        computer_choice = random.choice(choices)

        display_choices(user_choice, computer_choice)

        determine_winner(user_choice, computer_choice)
                
        proceed = input('Continue? (Yes/No): ').lower()
        if proceed == 'no' or proceed == 'n':
            print(thanks_string)
            input('Press Enter to continue...')
            break


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    play_game()


if __name__ == '__main__':
    main()
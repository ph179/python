# Beginner Projects

---
## Files

1. one_million.py

---
## Code and Video's

1. One Million:

How long does it take my computer to count to one million?

Answer: 4 seconds

My code:

```python
# ------ Document info ------

#   Filename: one_million.py
#   Author: Philippe Heyvaert
#   Created: 07th October 2024
#   Last modified: 
#   Purpose: How long does it take my computer to count to a million?


# ------ Imports ------


# ------ Constants & Variables ------


# ------ Functions ------


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    [print(number) for number in range(1, 1000001)]


if __name__ == '__main__':
    main()
```

![](img/one_million.mp4)
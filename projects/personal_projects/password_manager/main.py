# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 15th September 2024
#   Last modified: 
#   Purpose: Store passwords in a protected txt-file.


# ------ Imports ------
import os


# ------ Constants & Variables ------
input_text: str = 'Add a new password or view the existing passwords?\n'
input_text: str = input_text + '\tOptions: Add - View or Quit: '
error_text: str = 'This is not a valid option.'
error_input: str = 'Press Enter to continue and please try again...'
quit_choices: tuple = ('q', 'quit')
add_choices: tuple = ('a', 'add')
view_choices: tuple = ('v', 'view')


# ------ Functions ------
def clear() -> None:
    """
    Clear the terminal. This clear() function works for all systems:
        - GNU/Linux
        - MacOs
        - Windows
    
    :return:
    """
    os.system('cls' if os.name == 'nt' else 'clear')


def add() -> None:
    """
    Add a new password to the list.

    :return:
    """
    name: str = input('Account name: ')
    password: str = input('Password: ')


def view() -> None:
    """
    View existing passwords in the list.

    :return:
    """
    pass


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    while True:
        clear()
        mode = input(input_text).lower()
        if mode in quit_choices:
            break

        if mode == 'add':
            add()
        elif mode == 'view':
            view()
        else:
            clear()
            print(error_text)
            input(error_input)
            continue


if __name__ == '__main__':
    main()
# ------ Document info ------

#   Filename: main.py
#   Author: Philippe Heyvaert
#   Created: 26th September 2024
#   Last modified: 27th September 2024
#   Purpose: Terminal calculator.


# ------ Imports ------


# ------ Constants & Variables ------
numbers: dict = {
    1: '1st',
    2: '2nd'
}


# ------ Functions ------
def get_number(number: int) -> float:
    """
    Check if the user input is valid.
    
    :return: float
    """
    while True:
        operand = input(f'Input your {numbers[number]} number: ')

        try:
            return float(operand)
        except:
            print('Invalid number. Please, try again.')


# ------ Main program ------
def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    operand: float = get_number(1)
    operand_0: float = get_number(2)
    sign: str = input('Operation choice: ')

    result: float = 0

    if sign == "+":
        result = operand + operand_0
    elif sign == "-":
        result = operand - operand_0
    elif sign == "/":
        if operand_0 != 0:
            result = operand / operand_0
        else:
            print('Division by zero.')
    elif sign == "*":
        result = operand * operand_0
    else:
        print('Invalid sign.')

    print(result)


if __name__ == '__main__':
    main()
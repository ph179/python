def example_bool() -> None:
    """
    This function displays the bool type as an example.

    :return:
    """
    is_non_empty = bool("Hello")
    is_non_empty_1 = bool()
    is_zero = bool(0)
    is_zero_1 = bool(10)

    print(is_non_empty)
    print(is_non_empty_1)
    print(is_zero)
    print(is_zero_1)


def main() -> None:
    """
    This main() function runs all of the main program.

    :return:
    """
    example_bool()


if __name__ == '__main__':
    main()